CREATE TABLE IF NOT EXISTS public.human
(
    name character varying(255) COLLATE pg_catalog."default",
    surname character varying(255) COLLATE pg_catalog."default",
    patronymic character varying(255) COLLATE pg_catalog."default",
    "factAddress" character varying(255) COLLATE pg_catalog."default",
    birthday date,
    id_human_ bigint NOT NULL,
    "passportIssuerBy" character varying(255) COLLATE pg_catalog."default",
    "passportIssuerDate" date,
    "passportNumber" character varying(4) COLLATE pg_catalog."default",
    "passportSeries" character varying(6) COLLATE pg_catalog."default",
    CONSTRAINT human_pkey PRIMARY KEY (id_human_)
);