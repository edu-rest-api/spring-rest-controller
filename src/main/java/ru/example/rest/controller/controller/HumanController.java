package ru.example.rest.controller.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.example.rest.controller.dto.HumanVO;
import ru.example.rest.controller.service.HumanService;
import ru.example.rest.controller.vo.HumanQueryVO;
import ru.example.rest.controller.vo.HumanUpdateVO;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Validated
@RestController
@RequestMapping("/human")
@RequiredArgsConstructor
public class HumanController {
    private final HumanService humanService;

    @PostMapping
    public void save(@Valid @RequestBody HumanVO vO) {
        humanService.save(vO);
    }

    @DeleteMapping("/{id}")
    public void delete(@Valid @NotNull @PathVariable("id") Long id) {
        humanService.delete(id);
    }

    @PutMapping("/{id}")
    public void update(@Valid @NotNull @PathVariable("id") Long id,
                       @Valid @RequestBody HumanUpdateVO vO) {
        humanService.update(id, vO);
    }

    @PostMapping("/pagination")
    public List<HumanVO> pagination(@RequestBody HumanQueryVO HumanQueryVO) {
        return humanService.searchHuman(HumanQueryVO);
    }

    @GetMapping("/{id}")
    public HumanVO getById(@Valid @NotNull @PathVariable("id") Long id) {
        return humanService.getById(id);
    }
}
