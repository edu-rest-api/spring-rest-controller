package ru.example.rest.controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class RestControllerApplication {
	public static void main(String[] args){
		SpringApplication.run(RestControllerApplication.class, args);
	}
}
