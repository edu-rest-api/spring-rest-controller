package ru.example.rest.controller.dto;

import java.io.Serializable;
import java.time.LocalDate;
import lombok.Data;

/**
 *
 * @TableName human
 */
@Data
public class HumanVO implements Serializable {
    private Long idHuman;
    private String name;
    private String surname;
    private String patronymic;
    private String factAddress;
    private LocalDate birthday;
    private String passportIssuerBy;
    private LocalDate passportIssuerDate;
    private String passportNumber;
    private String passportSeries;
    private static final long serialVersionUID = 1L;
}