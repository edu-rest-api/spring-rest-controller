package ru.example.rest.controller.dto;

import lombok.Data;

@Data
public class Pagination {
    int pageSize;
}
