package ru.example.rest.controller.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import ru.example.rest.controller.dto.HumanVO;
import ru.example.rest.controller.vo.HumanQueryVO;
import ru.example.rest.controller.vo.HumanUpdateVO;

import java.util.List;

@Mapper
public interface HumanMapper {

    int deleteByPrimaryKey(Long id);

    int insert(HumanVO record);

    int insertSelective(HumanVO record);

    HumanVO selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(HumanUpdateVO record);

    int updateByPrimaryKey(HumanVO record);

    List<HumanVO> searchHuman(@Param("human") HumanQueryVO human, @Param("limits") Integer limits);

}
