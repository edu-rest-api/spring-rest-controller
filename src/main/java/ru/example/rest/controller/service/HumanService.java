package ru.example.rest.controller.service;

import ru.example.rest.controller.dto.HumanVO;
import ru.example.rest.controller.vo.HumanQueryVO;
import ru.example.rest.controller.vo.HumanUpdateVO;

import java.util.List;


public interface HumanService {
    public Long save(HumanVO vO);

    public void delete(Long id);

    public void update(Long id, HumanUpdateVO vO);

    public HumanVO getById(Long id);

    public List<HumanVO> searchHuman(HumanQueryVO human);

}
