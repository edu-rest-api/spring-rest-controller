package ru.example.rest.controller.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.stereotype.Service;
import ru.example.rest.controller.dto.HumanVO;
import ru.example.rest.controller.mapper.HumanMapper;
import ru.example.rest.controller.service.HumanService;
import ru.example.rest.controller.vo.HumanQueryVO;
import ru.example.rest.controller.vo.HumanUpdateVO;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;


@Service
@RequiredArgsConstructor
public class HumanServiceImpl implements HumanService {
    private final HumanMapper humanMapper;

    public Long save(HumanVO vO) {
        HumanVO bean = new HumanVO();
        BeanUtils.copyProperties(vO, bean);
        humanMapper.insert(bean);
        return bean.getIdHuman();
    }

    public void delete(Long id) {
        int affectRows = humanMapper.deleteByPrimaryKey(id);
        if (affectRows == 0) {
            throw new OptimisticLockingFailureException("Can't delete entity:" + id);
        }
    }

    public void update(Long id, HumanUpdateVO vO) {
        HumanVO bean = requireOne(id);
        BeanUtils.copyProperties(vO, bean);
        int affectRows = humanMapper.updateByPrimaryKeySelective(vO);
        if (affectRows == 0) {
            throw new OptimisticLockingFailureException("Can't update entity:" + id);
        }
    }

    public HumanVO getById(Long id) {
        HumanVO original = requireOne(id);
        return toDto(original);
    }

    public List<HumanVO> searchHuman(HumanQueryVO human) {
        return humanMapper.searchHuman(human, human.getPagination().getPageSize());
    }

    private HumanVO toDto(HumanVO original) {
        HumanVO bean = new HumanVO();
        BeanUtils.copyProperties(original, bean);
        return bean;
    }

    private HumanVO requireOne(Long id) {
        return Optional.ofNullable(humanMapper.selectByPrimaryKey(id))
                .orElseThrow(() -> new NoSuchElementException("Resource not found: " + id));
    }
}
